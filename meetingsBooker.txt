-- -----------------------------------------------------------------------------
--             G�n�ration d'une base de donn�es pour
--                      PROGRESS 9.1
--                        (27/11/2020 13:43:22)
-- -----------------------------------------------------------------------------
--      Nom de la base : MLR
--      Projet : WS_BojonLaura
--      Auteur : sio2d19
--      Date de derni�re modification : 20/11/2020 16:58:55
-- -----------------------------------------------------------------------------
-- -----------------------------------------------------------------------------
--       TABLE : CATEGORIE_LIEU
-- -----------------------------------------------------------------------------

CREATE TABLE CATEGORIE_LIEU
   (
    IDCATEG VARCHAR(128) NOT NULL ,
    LIBELLECATEG VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_CATEGORIE_LIEU PRIMARY KEY (IDCATEG) 
   );
-- -----------------------------------------------------------------------------
--       TABLE : UTILISATEUR
-- -----------------------------------------------------------------------------

CREATE TABLE UTILISATEUR
   (
    LOGIN VARCHAR(128) NOT NULL ,
    IDENT_HERITAGE_65 VARCHAR(128) NOT NULL ,
    PASSWORD VARCHAR(128) NOT NULL ,
    IDENT VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_UTILISATEUR PRIMARY KEY (LOGIN) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE UTILISATEUR
-- -----------------------------------------------------------------------------

CREATE UNIQUE INDEX I_FK_ENT_292_ENTREPRISE_2
     ON UTILISATEUR (IDENT_HERITAGE_65 ASC)
     ;

----------------------------------------------------------------------
--       TABLE : FACILITE
-- -----------------------------------------------------------------------------

CREATE TABLE FACILITE
   (
    IDFACILITE <UNDEF> NOT NULL ,
    LIBELLEFACITLITE <UNDEF> NOT NULL 
,   CONSTRAINT PK_FACILITE PRIMARY KEY (IDFACILITE) 
   );
-- -----------------------------------------------------------------------------
--       TABLE : LIEU
-- -----------------------------------------------------------------------------

CREATE TABLE LIEU
   (
    IDLIEU VARCHAR(5) NOT NULL ,
    IDCATEG VARCHAR(128) NOT NULL ,
    IDPHOTO VARCHAR(128) NOT NULL ,
    IDVILLE VARCHAR(128) NOT NULL ,
    IDENT VARCHAR(128) NOT NULL ,
    LIBELLELIEU VARCHAR(128) NOT NULL ,
    COORDX VARCHAR(128) NOT NULL ,
    COORDY VARCHAR(128) NOT NULL ,
    ANNULATIONGRATUITE VARCHAR(128) NOT NULL ,
    NBETOILES NUMBER(4) NOT NULL ,
    DESCRIPTIF VARCHAR(128) NOT NULL ,
    ADRESSE VARCHAR(25) NOT NULL 
,   CONSTRAINT PK_LIEU PRIMARY KEY (IDLIEU) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE LIEU
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_LIEU_CATEGORIE_LIEU
     ON LIEU (IDCATEG ASC)
     ;

CREATE UNIQUE INDEX I_FK_LIEU_PHOTO
     ON LIEU (IDPHOTO ASC)
     ;

CREATE  INDEX I_FK_LIEU_VILLE
     ON LIEU (IDVILLE ASC)
     ;

CREATE  INDEX I_FK_LIEU_ENTREPRISE
     ON LIEU (IDENT ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : PAYS
-- -----------------------------------------------------------------------------

CREATE TABLE PAYS
   (
    IDPAYS VARCHAR(128) NOT NULL ,
    NOMPAYS VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_PAYS PRIMARY KEY (IDPAYS) 
   );
-- -----------------------------------------------------------------------------
--       TABLE : DUREE
-- -----------------------------------------------------------------------------

CREATE TABLE DUREE
   (
    IDHORAIRE VARCHAR(25) NOT NULL ,
    LIBELLEH VARCHAR(25) NOT NULL 
,   CONSTRAINT PK_DUREE PRIMARY KEY (IDHORAIRE) 
   );
-- -----------------------------------------------------------------------------
--       TABLE : PHOTO
-- -----------------------------------------------------------------------------

CREATE TABLE PHOTO
   (
    IDPHOTO VARCHAR(128) NOT NULL ,
    IDSALLE VARCHAR(128) NOT NULL ,
    IDLIEU VARCHAR(5) NOT NULL ,
    CHEMINPHOTO VARCHAR(128) NOT NULL ,
    PRINCIPALEO_N VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_PHOTO PRIMARY KEY (IDPHOTO) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE PHOTO
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_PHOTO_SALLE
     ON PHOTO (IDSALLE ASC)
     ;

CREATE UNIQUE INDEX I_FK_PHOTO_LIEU
     ON PHOTO (IDLIEU ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : VILLE
-- -----------------------------------------------------------------------------

CREATE TABLE VILLE
   (
    IDVILLE VARCHAR(128) NOT NULL ,
    IDPAYS VARCHAR(128) NOT NULL ,
    NOMVILLE VARCHAR(128) NOT NULL ,
    CODEPOSTAL VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_VILLE PRIMARY KEY (IDVILLE) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE VILLE
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_VILLE_PAYS
     ON VILLE (IDPAYS ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : ROOM_LAYOUT
-- -----------------------------------------------------------------------------

CREATE TABLE ROOM_LAYOUT
   (
    IDLAYOUT VARCHAR(128) NOT NULL ,
    LIBELLELAYOUT VARCHAR(128) NOT NULL ,
    PICTOLAYOUT VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_ROOM_LAYOUT PRIMARY KEY (IDLAYOUT) 
   );
-- -----------------------------------------------------------------------------
--       TABLE : ENTREPRISE
-- -----------------------------------------------------------------------------

CREATE TABLE ENTREPRISE
   (
    IDENT VARCHAR(128) NOT NULL ,
    IDVILLE VARCHAR(128) NOT NULL ,
    NOMENT VARCHAR(128) NOT NULL ,
    ADRESSEENT VARCHAR(128) NOT NULL ,
    TELENT VARCHAR(128) NOT NULL ,
    EMAIL VARCHAR(128) NOT NULL ,
    NOM VARCHAR(128) NOT NULL ,
    PRENOM VARCHAR(128) NOT NULL ,
    CONTACTEO_N VARCHAR(128) NOT NULL ,
    ACONTACTEO_N VARCHAR(128) NOT NULL ,
    TYPEINSCIRPTION VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_ENTREPRISE PRIMARY KEY (IDENT) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE ENTREPRISE
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_ENTREPRISE_VILLE
     ON ENTREPRISE (IDVILLE ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : RESERVATION
-- -----------------------------------------------------------------------------

CREATE TABLE RESERVATION
   (
    IDRESA VARCHAR(5) NOT NULL ,
    IDHORAIRE VARCHAR(25) NOT NULL ,
    IDLAYOUT VARCHAR(128) NOT NULL ,
    IDSALLE VARCHAR(128) NOT NULL ,
    IDENT VARCHAR(128) NOT NULL ,
    NBCONVIVES INTEGER(3) NOT NULL ,
    DATEDEBUT DATE(8) NOT NULL ,
    PRIXTOTAL REAL(5,2) NOT NULL 
,   CONSTRAINT PK_RESERVATION PRIMARY KEY (IDRESA) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE RESERVATION
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_RESERVATION_DUREE
     ON RESERVATION (IDHORAIRE ASC)
     ;

CREATE  INDEX I_FK_RESERVATION_ROOM_LAYOUT
     ON RESERVATION (IDLAYOUT ASC)
     ;

CREATE  INDEX I_FK_RESERVATION_SALLE
     ON RESERVATION (IDSALLE ASC)
     ;

CREATE  INDEX I_FK_RESERVATION_ENTREPRISE
     ON RESERVATION (IDENT ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : SALLE
-- -----------------------------------------------------------------------------

CREATE TABLE SALLE
   (
    IDSALLE VARCHAR(128) NOT NULL ,
    IDLIEU VARCHAR(5) NOT NULL ,
    NOMSALLE VARCHAR(128) NOT NULL ,
    TARFI_DEMI_JOURNEE VARCHAR(128) NOT NULL ,
    LARGEUR NUMBER(4) NOT NULL ,
    LONGUEUR NUMBER(4) NOT NULL ,
    SURFACE REAL(5,2) NOT NULL ,
    HAUTEUR REAL(5,2) NOT NULL 
,   CONSTRAINT PK_SALLE PRIMARY KEY (IDSALLE) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE SALLE
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_SALLE_LIEU
     ON SALLE (IDLIEU ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : EQUIPEMENT
-- -----------------------------------------------------------------------------

CREATE TABLE EQUIPEMENT
   (
    IDEQUIPEMENT VARCHAR(128) NOT NULL ,
    LIEBELLEEQUIPEMENT VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_EQUIPEMENT PRIMARY KEY (IDEQUIPEMENT) 
   );
-- -----------------------------------------------------------------------------
--       TABLE : VIDEO
-- -----------------------------------------------------------------------------

CREATE TABLE VIDEO
   (
    IDVIDEO VARCHAR(128) NOT NULL ,
    IDSALLE VARCHAR(128) NOT NULL ,
    CHEMINVIDEO VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_VIDEO PRIMARY KEY (IDVIDEO) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE VIDEO
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_VIDEO_SALLE
     ON VIDEO (IDSALLE ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : PROPOSER
-- -----------------------------------------------------------------------------

CREATE TABLE PROPOSER
   (
    IDLIEU VARCHAR(5) NOT NULL ,
    IDFACILITE <UNDEF> NOT NULL 
,   CONSTRAINT PK_PROPOSER PRIMARY KEY (IDLIEU, IDFACILITE) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE PROPOSER
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_PROPOSER_LIEU
     ON PROPOSER (IDLIEU ASC)
     ;

CREATE  INDEX I_FK_PROPOSER_FACILITE
     ON PROPOSER (IDFACILITE ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : RESERVER
-- -----------------------------------------------------------------------------

CREATE TABLE RESERVER
   (
    IDEQUIPEMENT VARCHAR(128) NOT NULL ,
    IDRESA VARCHAR(5) NOT NULL 
,   CONSTRAINT PK_RESERVER PRIMARY KEY (IDEQUIPEMENT, IDRESA) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE RESERVER
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_RESERVER_EQUIPEMENT
     ON RESERVER (IDEQUIPEMENT ASC)
     ;

CREATE  INDEX I_FK_RESERVER_RESERVATION
     ON RESERVER (IDRESA ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : CONTACTER
-- -----------------------------------------------------------------------------

CREATE TABLE CONTACTER
   (
    LADATE DATE(8) NOT NULL ,
    LOGIN VARCHAR(128) NOT NULL ,
    IDENT VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_CONTACTER PRIMARY KEY (LADATE, LOGIN, IDENT) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE CONTACTER
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_CONTACTER_UTILISATEUR
     ON CONTACTER (LOGIN ASC)
     ;

CREATE  INDEX I_FK_CONTACTER_ENTREPRISE
     ON CONTACTER (IDENT ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : EQUIPER
-- -----------------------------------------------------------------------------

CREATE TABLE EQUIPER
   (
    IDSALLE VARCHAR(128) NOT NULL ,
    IDEQUIPEMENT VARCHAR(128) NOT NULL 
,   CONSTRAINT PK_EQUIPER PRIMARY KEY (IDSALLE, IDEQUIPEMENT) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE EQUIPER
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_EQUIPER_SALLE
     ON EQUIPER (IDSALLE ASC)
     ;

CREATE  INDEX I_FK_EQUIPER_EQUIPEMENT
     ON EQUIPER (IDEQUIPEMENT ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : AVOIR
-- -----------------------------------------------------------------------------

CREATE TABLE AVOIR
   (
    IDSALLE VARCHAR(128) NOT NULL ,
    IDLAYOUT VARCHAR(128) NOT NULL ,
    CAPACITE <UNDEF> NOT NULL 
,   CONSTRAINT PK_AVOIR PRIMARY KEY (IDSALLE, IDLAYOUT) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE AVOIR
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_AVOIR_SALLE
     ON AVOIR (IDSALLE ASC)
     ;

CREATE  INDEX I_FK_AVOIR_ROOM_LAYOUT
     ON AVOIR (IDLAYOUT ASC)
     ;

-- -----------------------------------------------------------------------------
--       TABLE : SELECTIONNER
-- -----------------------------------------------------------------------------

CREATE TABLE SELECTIONNER
   (
    IDFACILITE <UNDEF> NOT NULL ,
    IDRESA VARCHAR(5) NOT NULL 
,   CONSTRAINT PK_SELECTIONNER PRIMARY KEY (IDFACILITE, IDRESA) 
   );
-- -----------------------------------------------------------------------------
--       INDEX DE LA TABLE SELECTIONNER
-- -----------------------------------------------------------------------------

CREATE  INDEX I_FK_SELECTIONNER_FACILITE
     ON SELECTIONNER (IDFACILITE ASC)
     ;

CREATE  INDEX I_FK_SELECTIONNER_RESERVATION
     ON SELECTIONNER (IDRESA ASC)
     ;


-- -----------------------------------------------------------------------------
--       CREATION DES REFERENCES DE TABLE
-- -----------------------------------------------------------------------------


ALTER TABLE UTILISATEUR ADD (
     CONSTRAINT FK_ENT_292_ENTREPRISE_2
          FOREIGN KEY (IDENT_HERITAGE_65)
               REFERENCES ENTREPRISE (IDENT)) ;

ALTER TABLE LIEU ADD (
     CONSTRAINT FK_LIEU_CATEGORIE_LIEU
          FOREIGN KEY (IDCATEG)
               REFERENCES CATEGORIE_LIEU (IDCATEG)) ;

ALTER TABLE LIEU ADD (
     CONSTRAINT FK_LIEU_PHOTO
          FOREIGN KEY (IDPHOTO)
               REFERENCES PHOTO (IDPHOTO)) ;

ALTER TABLE LIEU ADD (
     CONSTRAINT FK_LIEU_VILLE
          FOREIGN KEY (IDVILLE)
               REFERENCES VILLE (IDVILLE)) ;

ALTER TABLE LIEU ADD (
     CONSTRAINT FK_LIEU_ENTREPRISE
          FOREIGN KEY (IDENT)
               REFERENCES ENTREPRISE (IDENT)) ;

ALTER TABLE PHOTO ADD (
     CONSTRAINT FK_PHOTO_SALLE
          FOREIGN KEY (IDSALLE)
               REFERENCES SALLE (IDSALLE)) ;

ALTER TABLE PHOTO ADD (
     CONSTRAINT FK_PHOTO_LIEU
          FOREIGN KEY (IDLIEU)
               REFERENCES LIEU (IDLIEU)) ;

ALTER TABLE VILLE ADD (
     CONSTRAINT FK_VILLE_PAYS
          FOREIGN KEY (IDPAYS)
               REFERENCES PAYS (IDPAYS)) ;

ALTER TABLE ENTREPRISE ADD (
     CONSTRAINT FK_ENTREPRISE_VILLE
          FOREIGN KEY (IDVILLE)
               REFERENCES VILLE (IDVILLE)) ;

ALTER TABLE RESERVATION ADD (
     CONSTRAINT FK_RESERVATION_DUREE
          FOREIGN KEY (IDHORAIRE)
               REFERENCES DUREE (IDHORAIRE)) ;

ALTER TABLE RESERVATION ADD (
     CONSTRAINT FK_RESERVATION_ROOM_LAYOUT
          FOREIGN KEY (IDLAYOUT)
               REFERENCES ROOM_LAYOUT (IDLAYOUT)) ;

ALTER TABLE RESERVATION ADD (
     CONSTRAINT FK_RESERVATION_SALLE
          FOREIGN KEY (IDSALLE)
               REFERENCES SALLE (IDSALLE)) ;

ALTER TABLE RESERVATION ADD (
     CONSTRAINT FK_RESERVATION_ENTREPRISE
          FOREIGN KEY (IDENT)
               REFERENCES ENTREPRISE (IDENT)) ;

ALTER TABLE SALLE ADD (
     CONSTRAINT FK_SALLE_LIEU
          FOREIGN KEY (IDLIEU)
               REFERENCES LIEU (IDLIEU)) ;

ALTER TABLE VIDEO ADD (
     CONSTRAINT FK_VIDEO_SALLE
          FOREIGN KEY (IDSALLE)
               REFERENCES SALLE (IDSALLE)) ;

ALTER TABLE PROPOSER ADD (
     CONSTRAINT FK_PROPOSER_LIEU
          FOREIGN KEY (IDLIEU)
               REFERENCES LIEU (IDLIEU)) ;

ALTER TABLE PROPOSER ADD (
     CONSTRAINT FK_PROPOSER_FACILITE
          FOREIGN KEY (IDFACILITE)
               REFERENCES FACILITE (IDFACILITE)) ;

ALTER TABLE RESERVER ADD (
     CONSTRAINT FK_RESERVER_EQUIPEMENT
          FOREIGN KEY (IDEQUIPEMENT)
               REFERENCES EQUIPEMENT (IDEQUIPEMENT)) ;

ALTER TABLE RESERVER ADD (
     CONSTRAINT FK_RESERVER_RESERVATION
          FOREIGN KEY (IDRESA)
               REFERENCES RESERVATION (IDRESA)) ;

ALTER TABLE CONTACTER ADD (
     CONSTRAINT FK_CONTACTER_LADATE
          FOREIGN KEY (LADATE)
               REFERENCES LADATE (LADATE)) ;

ALTER TABLE CONTACTER ADD (
     CONSTRAINT FK_CONTACTER_UTILISATEUR
          FOREIGN KEY (LOGIN)
               REFERENCES UTILISATEUR (LOGIN)) ;

ALTER TABLE CONTACTER ADD (
     CONSTRAINT FK_CONTACTER_ENTREPRISE
          FOREIGN KEY (IDENT)
               REFERENCES ENTREPRISE (IDENT)) ;

ALTER TABLE EQUIPER ADD (
     CONSTRAINT FK_EQUIPER_SALLE
          FOREIGN KEY (IDSALLE)
               REFERENCES SALLE (IDSALLE)) ;

ALTER TABLE EQUIPER ADD (
     CONSTRAINT FK_EQUIPER_EQUIPEMENT
          FOREIGN KEY (IDEQUIPEMENT)
               REFERENCES EQUIPEMENT (IDEQUIPEMENT)) ;

ALTER TABLE AVOIR ADD (
     CONSTRAINT FK_AVOIR_SALLE
          FOREIGN KEY (IDSALLE)
               REFERENCES SALLE (IDSALLE)) ;

ALTER TABLE AVOIR ADD (
     CONSTRAINT FK_AVOIR_ROOM_LAYOUT
          FOREIGN KEY (IDLAYOUT)
               REFERENCES ROOM_LAYOUT (IDLAYOUT)) ;

ALTER TABLE SELECTIONNER ADD (
     CONSTRAINT FK_SELECTIONNER_FACILITE
          FOREIGN KEY (IDFACILITE)
               REFERENCES FACILITE (IDFACILITE)) ;

ALTER TABLE SELECTIONNER ADD (
     CONSTRAINT FK_SELECTIONNER_RESERVATION
          FOREIGN KEY (IDRESA)
               REFERENCES RESERVATION (IDRESA)) ;


-- -----------------------------------------------------------------------------
--                FIN DE GENERATION
-- -----------------------------------------------------------------------------